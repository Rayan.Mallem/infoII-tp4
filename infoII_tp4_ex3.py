class File:
    def __init__(self):
        self.items = []

    def enqueue(self, priorite, item):
        entre = (priorite, item)
        if self.is_empty():
            self.items.append(entre)

        else:
            trouve = False
            for i in range(len(self.items)):
                if priorite > self.items[i][0]:
                    self.items.insert(i, entre)
                    trouve = True
                    break

            if not trouve:
                self.items.append(entre)

    def dequeue(self):
        if self.is_empty():
            return "La pile est vide !"
        else:
            return self.items.pop(0)

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)
